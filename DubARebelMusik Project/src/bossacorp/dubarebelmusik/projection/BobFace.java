package bossacorp.dubarebelmusik.projection;

import processing.core.PApplet;
import processing.core.PImage;
import bossacorp.dubarebelmusik.interfaces.IObserver;
import bossacorp.dubarebelmusik.interfaces.ISubject;
import bossacorp.dubarebelmusik.interfaces.InformacionDePaso;

public class BobFace implements IObserver {
	
	PImage bobPicture;
	String bcolor;
	PApplet parent;
	int xLocation;
	int alfaValue;
	int columns, rows;
	int cellsize = 3;
	int pinta;
	float z;
	int mouse;
	float rightHandx, rightHandy;
	String bobType;
	InformacionDePaso datos = new InformacionDePaso();
	
	public BobFace(PApplet p, String bobColor, ISubject subject){
		parent = p;
		bcolor = bobColor;
		alfaValue = 0;
		mouse = 0;
		subject.registerObserver(this);
		
		if  (bcolor == "VERDECara"){
			bobPicture = parent.loadImage("../../images/GreenSmokeCara.png");
			xLocation = 0;
			columns = bobPicture.width / cellsize;
			rows = bobPicture.height / cellsize;
		}
		if  (bcolor == "AMARILLOCara"){
			bobPicture = parent.loadImage("../../images/YellowSmokeCara.png");
			xLocation = 480;
			columns = bobPicture.width / cellsize;
			rows = bobPicture.height / cellsize;
			
		}
		if  (bcolor == "ROJOCara"){
			bobPicture = parent.loadImage("../../images/RedSmokeCara.png");
			xLocation = 960;
			columns = bobPicture.width / cellsize;
			rows = bobPicture.height / cellsize;
		}
	}
	@Override
	public void update(InformacionDePaso datosNotificados) {
		if(bcolor.equals(datosNotificados.getColor())){
			mouse = datosNotificados.getAlfaValue();	
		}
	}
	public void explode(){
	
		for(int ig = 0; ig <columns; ig ++){
			for(int jg = 0; jg < rows; jg ++){
				
				int Facex = ig*cellsize + cellsize/2;
				int Facey = jg*cellsize + cellsize/2;
				int loc = Facex + Facey * bobPicture.width;
				pinta = bobPicture.pixels[loc];
				z =   ((mouse / ((float)parent.width/3)) * parent.brightness(bobPicture.pixels[loc]) +1.0f);
				parent.pushMatrix();
				parent.translate(Facex+xLocation, Facey, z);
				parent.fill(pinta, mouse);
				parent.noStroke();
				parent.rectMode(parent.CENTER);
				parent.rect(0, 0, cellsize, cellsize);
				parent.popMatrix();
				}
			}
	}
}
