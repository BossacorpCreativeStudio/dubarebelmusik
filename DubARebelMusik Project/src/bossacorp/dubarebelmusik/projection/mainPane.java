package bossacorp.dubarebelmusik.projection;

import bossacorp.dubarebelmusik.fx.*;
import bossacorp.dubarebelmusik.midimanager.*;
import processing.core.*;

public class mainPane extends PApplet{

	Humo humoVerde, humoAmarillo, humoRojo,humo;
	OscManager oscReciever;
	BobImage greenBob, yellowBob, redBob,greenBobM;
	BobFace greenBobFace, yellowBobFace, redBobFace;
	OscManager sendData;
	BobFace RightHand;
	MidiManager midi;
	
	public void setup(){
		size(1440,800,P3D);
		
		oscReciever = OscManager.getInstance();
			
		
		greenBob = new BobImage(this, "VERDE",oscReciever);
		greenBobFace = new BobFace(this,"VERDECara", oscReciever);
		yellowBob = new BobImage(this, "AMARILLO",oscReciever);
		yellowBobFace = new BobFace(this, "AMARILLOCara",oscReciever);
		redBob = new BobImage(this, "ROJO",oscReciever);
		redBobFace = new BobFace(this, "ROJOCara",oscReciever);
	    sendData = new OscManager();
	    RightHand = new BobFace(this, "VERDECara", oscReciever);
		
		midi = MidiManager.getInstance();
		midi.valoresMidi();
		
		
		humoVerde = new Humo(this,-10,width/3,color(0,255,0),"VERDE",midi);
		humoAmarillo = new Humo(this,width/3,(width/3)*2, color(255,255,28),"AMARILLO",midi);
		humoRojo = new Humo(this, (width/3)*2,width,color(255,0,0),"ROJO",midi);

		

	}
	
	public void draw(){
		
		background(0);
		

		
		humoVerde.draw();
		humoAmarillo.draw();
		humoRojo.draw();
		
		greenBob.display();
		greenBobFace.explode();
		//greenBobFace.displayG();
		yellowBob.display();
		yellowBobFace.explode();
		//yellowBobFace.displayY();
		redBob.display();
		redBobFace.explode();
		//redBobFace.displayR();
		sendData.draw();
		
	
		
		/*humoAmarillo.addParticle();
		humoVerde.addParticle();
		humoRojo.addParticle();*/
	  }
	
	public static void main(String[] args) {
		System.out.println("Este es el punto de entrada de la aplicacion");
		PApplet.main(new String[] { "--present", "bossacorp.dubarebelmusik.projection.mainPane" });
	}
	
}
