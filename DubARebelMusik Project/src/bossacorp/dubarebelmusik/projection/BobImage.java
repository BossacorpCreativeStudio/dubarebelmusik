package bossacorp.dubarebelmusik.projection;


import bossacorp.dubarebelmusik.interfaces.IObserver;
import bossacorp.dubarebelmusik.interfaces.ISubject;
import bossacorp.dubarebelmusik.interfaces.InformacionDePaso;
import bossacorp.dubarebelmusik.midimanager.MidiManager;
import bossacorp.dubarebelmusik.midimanager.OscManager;
import processing.core.*;

public class BobImage implements IObserver {
	
	PImage allBob;
	String color;
	PApplet parent;
	int xLocation;
	int alfaValue;

	private ISubject subject;
	boolean reefMode = false;

	public BobImage(int alfaa){
		alfaValue=alfaa;
	}
	
	public BobImage(PApplet p, String bobColor, ISubject newSubject){
		parent = p;
		color = bobColor;
		alfaValue = 0;
		subject = newSubject;
		subject.registerObserver(this);
		
		if  (color == "VERDE"){
			allBob = parent.loadImage("../../images/GreenSmoke.jpg");
			xLocation = 0;
		}
		if  (color == "AMARILLO"){
			allBob = parent.loadImage("../../images/YellowSmoke.jpg");
			xLocation = 480;
		}
		if  (color == "ROJO"){
			allBob = parent.loadImage("../../images/RedSmoke.jpg");
			xLocation = 960;
		}
		
	}

	
	public void display(){
		//parent.image(bobFace, xLocation,0);
		parent.tint(255,alfaValue);
		parent.image(allBob, xLocation,0);
	}

	@Override
	public void update(InformacionDePaso datosNotificados) {

		if(datosNotificados.getTipoDeComando().equals("SWITCH")){
			
			System.out.println("Entered Switch IF - Reef Mode is"+reefMode);
			//Cambiar de Subject
			if(reefMode == false){ //Agregamos el subject Midi
				setSubject(MidiManager.getInstance());
				reefMode = true;
				
			}else{//Agregamos el subject OSC
				setSubject(OscManager.getInstance());
				reefMode = false;
			}
			//Switcheamos el reefMode
			
		}
		
		if(color.equals(datosNotificados.getColor())){
			alfaValue = datosNotificados.getAlfaValue();
			if(datosNotificados.getTipoDeComando().equals("note")){
				//System.out.println("HOLIS!!");
				if(datosNotificados.getVelocity()>0)
				alfaValue=datosNotificados.getVelocity();
			}
	
	}
		
		
	}
	
	public void setSubject(ISubject newSubject){
		//Nos desconectamos de un observer
		System.out.println("Nos desconectamos del sujeto");
		subject.removeObserver(this);
		
		//Nos conectamos al nuevo
		subject = newSubject;
		subject.registerObserver(this);
	}

}
