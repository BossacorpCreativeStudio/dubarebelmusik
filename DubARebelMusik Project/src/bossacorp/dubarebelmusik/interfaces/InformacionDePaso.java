package bossacorp.dubarebelmusik.interfaces;

import rwmidi.Note;

public class InformacionDePaso {
	
	String color,comando;
	int value,velocity,valueY;
	int n;
	
	public InformacionDePaso(){
		
	}
	
	public void setColor(String c) {
		color=c;
		
	}

	public void setAlfaValue(int v) {
		value=v;
		
	}

	public void setTipoDeComando(String com) {
		comando=com;
		
	}

	public int getAlfaValue() {
		return value;
	}

	public String getColor() {
		return color;
	}
	
	public String getTipoDeComando() {
		return comando;
	}

	public void setVelocity(int vel) {
		velocity=vel;
	}
	public int getVelocity() {
		return velocity;
	}

	public void setAlfaValuey(int valuey) {
		valueY=valuey;
		
	}
	
	public int getAlfaValuey(){
		return valueY;
	}

}
