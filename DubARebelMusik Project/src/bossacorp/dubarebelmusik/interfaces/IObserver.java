package bossacorp.dubarebelmusik.interfaces;

public interface IObserver {


	public void update(InformacionDePaso datosNotificados);

	//public void update(int newValue, String bobType);

}
