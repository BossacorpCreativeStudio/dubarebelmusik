package bossacorp.dubarebelmusik.fx;


import java.util.ArrayList;

import bossacorp.dubarebelmusik.interfaces.IObserver;
import bossacorp.dubarebelmusik.interfaces.ISubject;
import bossacorp.dubarebelmusik.interfaces.InformacionDePaso;
import processing.core.PApplet;

public class Humo implements IObserver {
	
	PApplet parent;
	int numParticles = 0; 
	int xInicio, xFin; //Estas son las posicion de donde a donde se va a dibujar
	int colorDeHumo;
	String stripeType;
	InformacionDePaso val;
	ArrayList<Particle> particles;
	
	public Humo(PApplet p, int xIni, int xEnd, int color, String colorType, ISubject midi){
		
		parent = p;
		particles = new ArrayList<Particle>();
		xInicio = xIni;
		xFin = xEnd;
		colorDeHumo = color;
		stripeType = colorType;
		midi.registerObserver(this);
		
	}
	
	public void draw() {	
		
		iniciarParticulas();
		moverParticulas();
	}
	
	public void iniciarParticulas(){
		for (int i = particles.size()-1; i >= 0; i--) {
		        Particle p = particles.get(i);
				if (numParticles < particles.size()) {
						    // posicion inicial de las particulas
					
					//TODO: Ojo no mandar llamar asi las variables, ponerle setters y getters
						    p.x = parent.random(xInicio,xFin);
						    p.y = parent.random(parent.height,parent.height+40); 
					
						    // mover aleatoriamente las particulas
						    p.vx = parent.random((float)-.25, (float).25);
						    p.vy = -4 + parent.random(-1, 1); //Cambiamos esto para que se muevan mas rapido
						    p.maxLifeTime += parent.random(-15, 15);

						    // agregar al arreglo particulas 
					        particles.get(numParticles).equals(p);
						    numParticles += 1;
					}
			  }
	}
	
	public void moverParticulas(){
		 for (int i = 0; i < numParticles; i++) {
			    // mover la posicion de la particula 
			    particles.get(i).update();

			    // verificar que la particula siga viva 
			    if (particles.get(i).lifeTime < particles.get(i).maxLifeTime) {
			      // pintar particula viva 
			    		particles.get(i).paint();
			    }
			  }
	}
	
	public void addParticle(){
		
		for(int i=0; i < 15; i++){
			particles.add(new Particle(parent,colorDeHumo));
		}
	}

	@Override
	public void update(InformacionDePaso datosNotificados) {
		// TODO Auto-generated method stub
		if (stripeType.equals(datosNotificados.getColor())){
			//System.out.println("Me llego un mensaje de "+ datosNotificados.getColor());
			if(datosNotificados.getTipoDeComando().equals("knob")){
				for(int i = 0 ; i < particles.size(); i++){
					particles.get(i).setAlfaValue(datosNotificados.getAlfaValue());
				}
			}
	    if(datosNotificados.getTipoDeComando().equals("note")){
				 //System.out.println("toque nota HUMO");
				 addParticle();
			 }	

     }
  }

	
}
