package bossacorp.dubarebelmusik.fx;

import processing.core.*;

	class Particle {
	  float x, y; // posicion de la particula 
	  float vx, vy; // velocidad de la particula
	  int lifeTime; // tiempo de vida de la particula
	  int maxLifeTime; // su tiempo maximo de vida 
	  PImage texture;
	  PApplet parent; 
	  int colorOfParticula;
	  int alfaValue;


	  /**
	   se inicia la particula con un tiempo de vida 
	   **/
	  Particle(PApplet f, int color) {
		parent = f;  
	    lifeTime = 0;
	    maxLifeTime = 200;
	    colorOfParticula = color;
	    alfaValue = 0;
	    texture = parent.loadImage("../../images/BigSmoke.png");
	  }
	  
	  Particle(){
		  
	  }

	  /**
	   mover la particula por la velocidad y aumentar su tiempo de vida
	   **/
	  void update() {
	    x += vx;
	    y += vy; 

	    lifeTime += 1;
	  }
	  
	  public void setAlfaValue(int v){
		  alfaValue = v;
	  }
	  
	  void paint() {

	    float alpha = (1 - (float)lifeTime / maxLifeTime)* 150+alfaValue;
	    parent.tint(colorOfParticula,alpha);
	    parent.image(texture,x,y);
	  }
	}


