package bossacorp.dubarebelmusik.midimanager;

import java.util.ArrayList;

import bossacorp.dubarebelmusik.interfaces.*;
import oscP5.*;
import netP5.*;
public class OscManager implements ISubject{

	ArrayList<IObserver> observers;
	OscP5 oscP5;
	NetAddress myRemoteLocation;
	
	private static OscManager instance = null;
	
	public static OscManager getInstance(){
		if(instance == null){
			instance = new OscManager();
		}
		return instance;
	}
	
	public OscManager(){
		oscP5 = new OscP5(this,12345);
		observers = new ArrayList<IObserver>();
		myRemoteLocation = new NetAddress("127.0.0.1", 12346);
	}
	public void sendData(){
		OscMessage myMessage = new OscMessage("/righthand_trackjointpos");
		OscMessage myMessage2 = new OscMessage("/head_trackjointpos");
		OscMessage myMessage3 = new OscMessage("/lefthand_trackjointpos");
		myMessage.add(3);
		myMessage2.add(3);
		myMessage3.add(3);
		oscP5.send(myMessage, myRemoteLocation);
		oscP5.send(myMessage2, myRemoteLocation);
		oscP5.send(myMessage3, myRemoteLocation);
	}
	
	public void draw(){
		sendData();
	}
	
	/* incoming osc message are forwarded to the oscEvent method. */
	public void oscEvent(OscMessage theOscMessage) {
	  /* print the address pattern and the typetag of the received OscMessage */
		//System.out.println("### received an osc message.");
		//System.out.println(" addrpattern:"+theOscMessage.addrPattern()+" value: "+theOscMessage.get(0).floatValue());
		//System.out.println(" typetag: "+theOscMessage.typetag());
		
		if(theOscMessage.addrPattern().equals(GlobalSettings.GREEN_FADER)) {
			InformacionDePaso datosNotificados = new InformacionDePaso();
			datosNotificados.setAlfaValue((int)theOscMessage.get(0).floatValue());
			datosNotificados.setColor("VERDE");
			datosNotificados.setTipoDeComando("none");
			
			  notifyObservers(datosNotificados);
		}
		//Efecto del Kinect con Explode Verde
		if(theOscMessage.addrPattern().equals("/lefthand_pos_screen")) {
			System.out.println("Estoy implrimiendo algo");
			InformacionDePaso datosNotificados = new InformacionDePaso();
			  datosNotificados.setColor("VERDECara");
			  datosNotificados.setAlfaValue((int) theOscMessage.get(0).floatValue());
			  datosNotificados.setAlfaValuey((int) theOscMessage.get(1).floatValue());
			  datosNotificados.setTipoDeComando("fader");

			  notifyObservers(datosNotificados);
		}
		if(theOscMessage.addrPattern().equals(GlobalSettings.GREEN_FADER_FACE)) {
			InformacionDePaso datosNotificados = new InformacionDePaso();
			  datosNotificados.setColor("VERDECara");
			  datosNotificados.setAlfaValue((int)theOscMessage.get(0).floatValue());
			  datosNotificados.setTipoDeComando("fader");

			  notifyObservers(datosNotificados);
		}
		if(theOscMessage.addrPattern().equals(GlobalSettings.YELLOW_FADER)) {
			InformacionDePaso datosNotificados = new InformacionDePaso();
			datosNotificados.setAlfaValue((int)theOscMessage.get(0).floatValue());
			datosNotificados.setColor("AMARILLO");
			datosNotificados.setTipoDeComando("none");
			
			notifyObservers(datosNotificados);
			
		}
		if(theOscMessage.addrPattern().equals("/head_pos_screen")) {
			System.out.println("Estoy implrimiendo algo");
			InformacionDePaso datosNotificados = new InformacionDePaso();
			  datosNotificados.setColor("AMARILLOCara");
			  datosNotificados.setAlfaValue((int) theOscMessage.get(0).floatValue());
			  datosNotificados.setAlfaValuey((int) theOscMessage.get(1).floatValue());
			  datosNotificados.setTipoDeComando("fader");

			  notifyObservers(datosNotificados);
		}
		if(theOscMessage.addrPattern().equals(GlobalSettings.YELLOW_FADER_FACE)) {
			InformacionDePaso datosNotificados = new InformacionDePaso();
			  datosNotificados.setColor("AMARILLOCara");
			  datosNotificados.setAlfaValue((int)theOscMessage.get(0).floatValue());
			  datosNotificados.setTipoDeComando("fader");

			  notifyObservers(datosNotificados);
		}
		if(theOscMessage.addrPattern().equals(GlobalSettings.RED_FADER)) {
			InformacionDePaso datosNotificados = new InformacionDePaso();
			datosNotificados.setAlfaValue((int)theOscMessage.get(0).floatValue());
			datosNotificados.setColor("ROJO");
			datosNotificados.setTipoDeComando("none");
			notifyObservers(datosNotificados);
		}
		if(theOscMessage.addrPattern().equals("/righthand_pos_screen")) {
			System.out.println("Estoy implrimiendo algo");
			InformacionDePaso datosNotificados = new InformacionDePaso();
			  datosNotificados.setColor("ROJOCara");
			  datosNotificados.setAlfaValue((int) theOscMessage.get(0).floatValue());
			  datosNotificados.setAlfaValuey((int) theOscMessage.get(1).floatValue());
			  datosNotificados.setTipoDeComando("fader");

			  notifyObservers(datosNotificados);
		}
		if(theOscMessage.addrPattern().equals(GlobalSettings.RED_FADER_FACE)) {
			InformacionDePaso datosNotificados = new InformacionDePaso();
			  datosNotificados.setColor("ROJOCara");
			  datosNotificados.setAlfaValue((int)theOscMessage.get(0).floatValue());
			  datosNotificados.setTipoDeComando("fader");

			  notifyObservers(datosNotificados);
			//notifyObservers((int)theOscMessage.get(0).floatValue(),"ROJOCara");
		}
		
		//BobImages must switch Subjects
		if(theOscMessage.addrPattern().equals(GlobalSettings.MODE_SWITCHER)){
			System.out.println("<---- Someone order a Mode SWITCH --->");
			
			InformacionDePaso datosNotificados = new InformacionDePaso();
			  datosNotificados.setColor("ALL");
			  datosNotificados.setAlfaValue((int)theOscMessage.get(0).floatValue());
			  //System.out.println("AlfaValue "+datosNotificados.getAlfaValue());
			  datosNotificados.setTipoDeComando("SWITCH");

			  notifyObservers(datosNotificados);
			//notifyObservers((int)theOscMessage.get(0).floatValue(),"ROJOCara");
			
			
		}
		
	}
		

	@Override
	public void registerObserver(IObserver o) {
		observers.add(o);
		System.out.println("Soy OSC tengo"+observers.size()+" weyes escuchandome");
		
	}

	@Override
	public void removeObserver(IObserver o) {
		System.out.println("soy OSC y se esta YENDO un wey de aqui");
		
		int i = observers.indexOf(o);
		if (i >= 0) observers.remove(i);		
	}

	@Override
	public void notifyObservers(InformacionDePaso datosNotificados) {
		for(int i = 0; i < observers.size() ; i++){
			IObserver obs = (IObserver)observers.get(i);
			obs.update(datosNotificados);
		}
		
	}

	
}
