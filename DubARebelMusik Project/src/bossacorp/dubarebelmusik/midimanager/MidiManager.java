package bossacorp.dubarebelmusik.midimanager;

import java.util.ArrayList;

import processing.core.PApplet;
import bossacorp.dubarebelmusik.fx.Humo;
import bossacorp.dubarebelmusik.interfaces.GlobalSettings;
import bossacorp.dubarebelmusik.interfaces.IObserver;
import bossacorp.dubarebelmusik.interfaces.ISubject;
import bossacorp.dubarebelmusik.interfaces.InformacionDePaso;
import rwmidi.*;


public class MidiManager implements ISubject {
	
	int value = 0,number = 0;
	MidiInput input;
	MidiOutput output;
	Note note;
	int maxNote = 84;
	int minNote = 36;
	int numNotes = maxNote-minNote;
	int notePlayed = (maxNote+minNote)/2;
	Humo humoVerde, humoAmarillo, humoRojo;
	PApplet parent;
	ArrayList<IObserver> observers;
	
	private static MidiManager instance = null;
	
	public static MidiManager getInstance(){
		if(instance==null){
			instance = new MidiManager();
		}
		return instance;
	}
	
	public MidiManager(){
		observers = new ArrayList<IObserver>();
		
	}
	

	public void controllerChangeReceived(Controller controller){
		  number = controller.getCC();
		  value = controller.getValue();
		  System.out.println("CC: " + controller.getCC() + ", value: " + controller.getValue());
		 if (number == 71) {
			//  notifyObservers(value,"VERDE");
				InformacionDePaso datosNotificados = new InformacionDePaso();
			  datosNotificados.setColor("VERDE");
			  datosNotificados.setAlfaValue(controller.getValue());
			  datosNotificados.setTipoDeComando("knob");
			  notifyObservers(datosNotificados);
		  	}
		  if(number == 14){
			 // notifyObservers(value,"AMARILLO");
				InformacionDePaso datosNotificados = new InformacionDePaso();
				  datosNotificados.setColor("AMARILLO");
				  datosNotificados.setAlfaValue(controller.getValue());
				  datosNotificados.setTipoDeComando("knob");
				  notifyObservers(datosNotificados);
		  }
		  if(number == 16){
			 // notifyObservers(value,"ROJO");
				InformacionDePaso datosNotificados = new InformacionDePaso();
				  datosNotificados.setColor("ROJO");
				  datosNotificados.setAlfaValue(controller.getValue());
				  datosNotificados.setTipoDeComando("knob");
				  notifyObservers(datosNotificados);
		  }
		  
		//BobImages must switch Subjects
			if(number == 76){
				System.out.println("<---- Someone order a Mode SWITCH --->");
				
				InformacionDePaso datosNotificados = new InformacionDePaso();
				  datosNotificados.setColor("ALL");
				 //datosNotificados.setAlfaValue((int)theOscMessage.get(0).floatValue());
				  datosNotificados.setAlfaValue(controller.getValue());
				  //System.out.println("AlfaValue "+datosNotificados.getAlfaValue());
				  datosNotificados.setTipoDeComando("SWITCH");

				  notifyObservers(datosNotificados);
				//notifyObservers((int)theOscMessage.get(0).floatValue(),"ROJOCara");			
				
			}

		  }
	
	public void valoresMidi(){
		
	    input = RWMidi.getInputDevices()[0].createInput(this);
	    output = RWMidi.getOutputDevices()[0].createOutput();
	    System.out.println("Input: " + input.getName());
	    System.out.println("Output: " + output.getName());
	}
	
	public void noteOnReceived(Note note) {
		  notePlayed = note.getPitch();
		  //System.out.println("Holis->"+note.getPitch());
		 // System.out.println("Note on: " + note.getPitch() + ", velocity: " + note.getVelocity());
			 if (notePlayed == 37 || notePlayed ==  49 || notePlayed == 61) {
				  //notifyObservers(value,"VERDE");
				 InformacionDePaso datosNotificados = new InformacionDePaso();
				  datosNotificados.setColor("VERDE");
				  datosNotificados.setAlfaValue(0);
				  datosNotificados.setVelocity(note.getVelocity());
				  datosNotificados.setTipoDeComando("note");
				  notifyObservers(datosNotificados);
			  	}
			 if (notePlayed == 40 || notePlayed == 52 || notePlayed == 64 || notePlayed == 45 || notePlayed == 57 || notePlayed == 69) {
				 // notifyObservers(value,"AMARILLO");
				  InformacionDePaso datosNotificados = new InformacionDePaso();
				  datosNotificados.setColor("AMARILLO");
				  datosNotificados.setAlfaValue(0);
				  datosNotificados.setVelocity(note.getVelocity());
				  datosNotificados.setTipoDeComando("note");
				  notifyObservers(datosNotificados);
			  	}
			 if (notePlayed == 42 || notePlayed == 54 || notePlayed == 66) {
				  //notifyObservers(value,"ROJO");
				  InformacionDePaso datosNotificados = new InformacionDePaso();
				  datosNotificados.setColor("ROJO");
				  datosNotificados.setAlfaValue(0);
				  datosNotificados.setVelocity(note.getVelocity());
				  datosNotificados.setTipoDeComando("note");
				  notifyObservers(datosNotificados);
			  	}
		}

		void noteOffReceived(Note note) {
			System.out.println("Note off: " + note.getPitch());
		}
	

	@Override
	public void registerObserver(IObserver o) {
		observers.add(o);
		System.out.println("Soy MIDI y se esta REGISTRANDO un wey conmigo");
	}

	@Override
	public void removeObserver(IObserver o) {
		int i = observers.indexOf(o);
		if (i >= 0) observers.remove(i);	
		System.out.println("Soy MIDI y se esta YENDO un wey");
	}
	
	public void notifyObservers(InformacionDePaso datosNotificados) {
		for(int i = 0 ; i < observers.size(); i++){
			IObserver obs = observers.get(i);
			obs.update(datosNotificados);
		}
		
	}

}
